# Purpose:
This is a final project for Math 482, a Combinatorial Algorithms course. Students were randomly given the name of a problem in the subject. I recieved, "k-center clustering"; detailed information on this problem can be found in my report. We were required to write a report, as well as prepare a 5 minute presentation in order to teach the rest of the class the solution to the problem.  

# Contents: 

## Top:
The presentation pdf contains the slides for my short lecture.
The kCenterClustering.pdf file contains the final report.
The HTML files contains a snapshot of the particular notebook instance I used for the reports.

## Programs:
The solution for this problem is a non-deterministic Greedy Algorithm.
The ipynb files are ipython notebook files to the corresponding .py files
The python files are raw solutions to the problem, in 2D and 3D; I wrote both in order to choose the solution that looked most aesthetic. 

# Notes
1. My example includes a fourth cluster to introduce noise data, just to see how the algorithm would perform.
2. I think cluster assigning is an excellent place to apply parallel computation for very large datasets.
